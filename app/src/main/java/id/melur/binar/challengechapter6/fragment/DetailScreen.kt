package id.melur.binar.challengechapter6.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import id.melur.binar.challengechapter6.model.Status
import com.bumptech.glide.Glide
import id.melur.binar.challengechapter6.BuildConfig
import id.melur.binar.challengechapter6.database.Favorite
import id.melur.binar.challengechapter6.database.FavoriteDatabase
import id.melur.binar.challengechapter6.viewmodel.ViewModel
import id.melur.binar.challengechapter6.databinding.FragmentDetailScreenBinding
import id.melur.binar.challengechapter6.helper.MovieRepo
import id.melur.binar.challengechapter6.helper.UserRepo
import id.melur.binar.challengechapter6.helper.viewModelsFactory
import id.melur.binar.challengechapter6.service.TMDBApiService
import id.melur.binar.challengechapter6.service.TMDBClient
import id.melur.binar.challengechapter6.viewmodel.MovieViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailScreen : Fragment() {

    private var _binding: FragmentDetailScreenBinding? = null
    private val binding get() = _binding!!

    private lateinit var sharedPref: SharedPreferences
    private var dataUsername: String? = ""

    private val userRepo : UserRepo by lazy { UserRepo(requireContext()) }

    private val apiService : TMDBApiService by lazy { TMDBClient.instance }
    private val viewModel: ViewModel by viewModelsFactory { ViewModel(userRepo, apiService) }

    private val movieRepository: MovieRepo by lazy { MovieRepo(apiService) }
    private val movieViewModel: MovieViewModel by viewModelsFactory { MovieViewModel(movieRepository) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentDetailScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        sharedPref = context.getSharedPreferences("username", Context.MODE_PRIVATE)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val movieId = arguments?.getInt("id")
        val username = sharedPref.getString("username", "")
        observeMovie(movieId!!)
        buttonColor(username, movieId)
        favoriteButtonOnPressed(username, movieId)
    }


    private fun observeMovie(movieId: Int) {
        movieViewModel.getDetailMovie(movieId).observe(viewLifecycleOwner) {
            when (it.status) {
                Status.SUCCESS -> {
                    binding.apply {
                        Glide.with(requireContext())
                            .load("https://www.themoviedb.org/t/p/w1920_and_h800_multi_faces/" + it.data!!.backdropPath)
                            .into(ivBackdrop)
                        Glide.with(requireContext())
                            .load("https://www.themoviedb.org/t/p/w220_and_h330_face/" + it.data.posterPath)
                            .into(ivPoster)

                        tvTitle.text = it.data.title
                        tvDate.text = it.data.releaseDate
                        tvOverview.text = it.data.overview
                        pbMovie.isVisible = false
                    }
                }
                Status.ERROR -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                }
            }
        }

    }

    private fun buttonColor(username: String?, movieId: Int) {
        viewModel.checkRegisteredFavorite(username, movieId)
        Handler(Looper.getMainLooper()).postDelayed({
            if (viewModel.isFav.value == true) {
                binding.btnFavorite.isVisible = false
                binding.btnRedFavorite.isVisible = true
            } else {
                binding.btnFavorite.isVisible = true
                binding.btnRedFavorite.isVisible = false
            }
        }, 200)
    }

    private fun favoriteButtonOnPressed(username: String?, movieId: Int) {
        binding.btnFavorite.setOnClickListener {
            viewModel.addDataFavorite(username, movieId)
            binding.btnFavorite.isVisible = false
            binding.btnRedFavorite.isVisible = true
            Toast.makeText(requireContext(), "Film telah ditambahkan kedalam list Favorit", Toast.LENGTH_SHORT).show()
        }
        binding.btnRedFavorite.setOnClickListener {
            viewModel.deleteDataFavorite(username, movieId)
            binding.btnFavorite.isVisible = true
            binding.btnRedFavorite.isVisible = false
            Toast.makeText(requireContext(), "Film telah dihapus dari list Favorit", Toast.LENGTH_SHORT).show()
        }
    }

}