package id.melur.binar.challengechapter6.fragment

import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import id.melur.binar.challengechapter6.R
import id.melur.binar.challengechapter6.viewmodel.ViewModel
import id.melur.binar.challengechapter6.databinding.FragmentRegisterScreenBinding
import id.melur.binar.challengechapter6.helper.UserRepo
import id.melur.binar.challengechapter6.helper.viewModelsFactory
import id.melur.binar.challengechapter6.service.TMDBApiService
import id.melur.binar.challengechapter6.service.TMDBClient

class RegisterScreen : Fragment() {

    private var _binding: FragmentRegisterScreenBinding? = null
    private val binding get() = _binding!!
    private lateinit var sharedPref: SharedPreferences
    private var dataUser: String? = ""

    private val userRepo : UserRepo by lazy { UserRepo(requireContext()) }

    private val api: TMDBApiService by lazy { TMDBClient.instance }
    private val viewModel: ViewModel by viewModelsFactory { ViewModel(userRepo, api) }

    //    private lateinit var userAdapter: UserAdapter
//    private var mDb: UserDatabase? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentRegisterScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        mDb = UserDatabase.getInstance(requireContext())
//        getDataFromDb()
        registerButtonOnPressed()
    }

    private fun registerButtonOnPressed() {
        binding.btnRegister.setOnClickListener {

            val username = binding.inputUsername.text.toString()
            val email = binding.inputEmail.text.toString()
            val password = binding.inputPassword.text.toString()
            val password1 = binding.inputConfirmPass.text.toString()

            if (username != "" && email != "" && password != "" && password1 != "") {
                if (password == password1) {
                    viewModel.addDataUser(username, email, password)
                    Toast.makeText(requireContext(), "Berhasil Registrasi", Toast.LENGTH_SHORT).show()
                    findNavController().navigate(R.id.action_registerScreen_to_loginScreen)
                }
                if (password != password1) {
                    Toast.makeText(requireContext(), "Password dan Confirm Password tidak sama", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(requireContext(), "Field tidak boleh kosong", Toast.LENGTH_SHORT).show()
            }
        }
    }
}