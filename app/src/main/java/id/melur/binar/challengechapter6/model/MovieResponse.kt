package id.melur.binar.challengechapter6.model

import com.google.gson.annotations.SerializedName

data class MovieResponse(

    @field:SerializedName("results")
    val results: List<Movie>
)