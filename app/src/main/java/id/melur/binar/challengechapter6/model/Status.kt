package id.melur.binar.challengechapter6.model


enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}