package id.melur.binar.challengechapter6.fragment

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import id.melur.binar.challengechapter6.R
import id.melur.binar.challengechapter6.viewmodel.ViewModel
import id.melur.binar.challengechapter6.databinding.FragmentLoginScreenBinding
import id.melur.binar.challengechapter6.datastore.LoggedDataStoreManager
import id.melur.binar.challengechapter6.helper.UserRepo
import id.melur.binar.challengechapter6.helper.viewModelsFactory
import id.melur.binar.challengechapter6.service.TMDBApiService
import id.melur.binar.challengechapter6.service.TMDBClient
import id.melur.binar.challengechapter6.viewmodel.LoggedViewModel

class LoginScreen : Fragment() {
    private var _binding: FragmentLoginScreenBinding? = null
    private val binding get() = _binding!!
    private lateinit var sharedPref: SharedPreferences
    private var dataUser: String? = ""

    private val userRepo : UserRepo by lazy { UserRepo(requireContext()) }

    private val api: TMDBApiService by lazy { TMDBClient.instance }
    private val viewModel: ViewModel by viewModelsFactory { ViewModel(userRepo, api) }

    private val pref: LoggedDataStoreManager by lazy { LoggedDataStoreManager(requireContext()) }

//    private val viewModel: ViewModel by viewModelsFactory { UserViewModel(userRepository) }
    private val loggedModel: LoggedViewModel by viewModelsFactory { LoggedViewModel(pref) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentLoginScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedPref = requireContext().getSharedPreferences("username", Context.MODE_PRIVATE)
        dataUser = sharedPref.getString("username", "")
        loginButtonOnPressed()
        regisButtonOnPressed()
        observeData()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        sharedPref = context.getSharedPreferences("username", Context.MODE_PRIVATE)
    }

    private fun regisButtonOnPressed() {
        binding.tvRegister.setOnClickListener {
            findNavController().navigate(R.id.action_loginScreen_to_registerScreen)
        }
    }

    private fun loginButtonOnPressed() {
        binding.btnLogin.setOnClickListener {
            val username = binding.etUsername.text.toString()
            val password = binding.etPassword.text.toString()
            if (username != "" && password != "") {
                viewModel.checkRegisteredUser(username, password)
                Handler(Looper.getMainLooper()).postDelayed({
                    // This method will be executed once the timer is over
                    if (viewModel.isLogin.value == true) {
                        Toast.makeText(requireContext(), "Login Berhasil", Toast.LENGTH_SHORT).show()
                        val editor = sharedPref.edit()
                        editor.putString("username", username)
                        editor.apply()
                        viewModel.checkLogin(username, password)
                        findNavController().navigate(R.id.action_loginScreen_to_homeScreen)
                    } else {
                        Toast.makeText(requireContext(), "Email atau Password tidak sesuai", Toast.LENGTH_SHORT).show()
                    }
                },200)
            } else {
                Toast.makeText(requireContext(), "Field tidak boleh kosong", Toast.LENGTH_SHORT).show()
            }
        }
    }


    private fun observeData() {
//        val intent = Intent(this@LoginScreen.requireContext(), HomeActivity::class.java)

        viewModel.notRegistered.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), "Email belum terdaftar", Toast.LENGTH_SHORT).show()
        }

        viewModel.validateEmailPassword.observe(viewLifecycleOwner) {
            if (it != null) {
                it.userId?.let { it1 -> loggedModel.saveData(it1, it.username, it.email, it.password) }
            }
//            startActivity(intent)
//            requireActivity().finish()
        }
    }
}