package id.melur.binar.challengechapter6.database

import androidx.room.*

@Dao
interface UserDao {

    @Query("SELECT * FROM User WHERE username = :query")
    fun getUser2(query: String): User

    //    @Query("SELECT name, dateOfBirth, address FROM User WHERE username = :username")
    @Query("SELECT * FROM User WHERE username = :username")
    fun coba(username: String) : List<User>

    @Query("SELECT * FROM User WHERE username = :username AND password = :password")
    fun getRegisteredUser(username: String, password: String) : List<User>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User) : Long

    @Query("UPDATE User SET name = :name, dateOfBirth = :dateOfBirth, address = :address WHERE username = :username")
    fun updateUser(username: String, name: String, dateOfBirth: String, address: String) : Int

}