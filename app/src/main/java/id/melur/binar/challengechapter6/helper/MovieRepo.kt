package id.melur.binar.challengechapter6.helper

import id.melur.binar.challengechapter6.service.TMDBApiService

class MovieRepo(private val apiService: TMDBApiService) {
    suspend fun getMovie(key: String) = apiService.getAllMovie(key)

    suspend fun getDetailMovie(movieId: Int, key: String) = apiService.getMovieDetail(movieId, key)
}