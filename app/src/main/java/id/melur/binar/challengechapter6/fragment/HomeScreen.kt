package id.melur.binar.challengechapter6.fragment

import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import id.melur.binar.challengechapter6.BuildConfig
import id.melur.binar.challengechapter6.R
import id.melur.binar.challengechapter6.model.Status
import id.melur.binar.challengechapter6.viewmodel.ViewModel
import id.melur.binar.challengechapter6.adapter.MovieAdapter
import id.melur.binar.challengechapter6.databinding.FragmentHomeScreenBinding
import id.melur.binar.challengechapter6.datastore.LoggedDataStoreManager
import id.melur.binar.challengechapter6.helper.MovieRepo
import id.melur.binar.challengechapter6.helper.UserRepo
import id.melur.binar.challengechapter6.helper.viewModelsFactory
import id.melur.binar.challengechapter6.model.MoviePopular
import id.melur.binar.challengechapter6.model.MoviePopularItem
import id.melur.binar.challengechapter6.service.TMDBApiService
import id.melur.binar.challengechapter6.service.TMDBClient
import id.melur.binar.challengechapter6.viewmodel.LoggedViewModel
import id.melur.binar.challengechapter6.viewmodel.MovieViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeScreen : Fragment() {

    private var _binding: FragmentHomeScreenBinding? = null
    private val binding get() = _binding!!

    private lateinit var movieAdapter: MovieAdapter

    private lateinit var sharedPref: SharedPreferences

    private val userRepo : UserRepo by lazy { UserRepo(requireContext()) }

    private val apiService : TMDBApiService by lazy { TMDBClient.instance }
    private val viewModel: ViewModel by viewModelsFactory { ViewModel(userRepo, apiService) }

    private val movieRepository: MovieRepo by lazy { MovieRepo(apiService) }
    private val movieViewModel: MovieViewModel by viewModelsFactory { MovieViewModel(movieRepository) }


    private val pref: LoggedDataStoreManager by lazy { LoggedDataStoreManager(requireContext()) }
    private val loggedViewModel: LoggedViewModel by viewModelsFactory { LoggedViewModel(pref) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentHomeScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedPref = requireContext().getSharedPreferences("username", Context.MODE_PRIVATE)

        val username = sharedPref.getString("username", "")
        binding.tvWelcome.text = "Welcome, $username!"

//        viewModel.test(username)
//        val test = viewModel.test.value.toString()
//        Toast.makeText(requireContext(), "$test", Toast.LENGTH_SHORT).show()

        observer()
        initRecyclerView()
        profileButtonOnPressed()
        favoriteButtonOnPressed()
    }

    private fun initRecyclerView() {
        movieAdapter = MovieAdapter { id: Int, movie: MoviePopularItem ->
            val bundle = Bundle()
            bundle.putInt("id", id)
            findNavController().navigate(R.id.action_homeScreen_to_detailScreen, bundle)
        }
        binding.rvData.apply {
            adapter = movieAdapter
//            layoutManager = GridLayoutManager(requireContext(), 2)
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun profileButtonOnPressed(){
        binding.btnProfile.setOnClickListener {
            findNavController().navigate(R.id.action_homeScreen_to_profileScreen)
        }
    }

    private fun favoriteButtonOnPressed() {
        binding.btnFavorite.setOnClickListener {
            findNavController().navigate(R.id.action_homeScreen_to_favoriteScreen)
        }
    }

    private fun observer() {
        movieViewModel.getAllMovie().observe(viewLifecycleOwner) {
            when (it.status) {
                Status.SUCCESS -> {
                    movieAdapter.updateData(it.data)
                    binding.pbMovie.isVisible = false
                }
                Status.ERROR -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                }
            }
        }
        viewModel.user2.observe(viewLifecycleOwner) {
            binding.apply {
//                tvWelcome.text = "Welcome, ${it?.username}!"
//                if (it?.image != "") {
//                    profileImage.setImageURI(Uri.parse(it?.image))
//                } else {
//                    profileImage.setImageResource(R.drawable.ic_profil)
//                }
            }
        }
        loggedViewModel.getEmail().observe(viewLifecycleOwner) {
            viewModel.getUser2(it)
        }
    }
}