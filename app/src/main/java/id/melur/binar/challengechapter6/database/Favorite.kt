package id.melur.binar.challengechapter6.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Favorite(
    @PrimaryKey(autoGenerate = true) val favoriteId : Int?,
    @ColumnInfo(name = "username") val username: String?,
    @ColumnInfo(name = "movieId") val movieId: Int
)