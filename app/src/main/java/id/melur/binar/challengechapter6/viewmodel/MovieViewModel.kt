package id.melur.binar.challengechapter6.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import id.melur.binar.challengechapter6.helper.MovieRepo
import id.melur.binar.challengechapter6.model.Resource
import kotlinx.coroutines.Dispatchers


class MovieViewModel(private val repository: MovieRepo) : ViewModel() {

    fun getAllMovie() = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(repository.getMovie("dabe4a0a4b3b05a755e4b510103fce91")))
        } catch (e: Exception) {
            emit(Resource.error(data = null, message = e.message ?: "Error Occurred!"))
        }
    }

    fun getDetailMovie(movieId: Int) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(
                Resource.success(
                    repository.getDetailMovie(
                        movieId,
                        "dabe4a0a4b3b05a755e4b510103fce91"
                    )
                )
            )
        } catch (e: Exception) {
            emit(Resource.error(data = null, message = e.message ?: "Error Occurred!"))
        }
    }
}