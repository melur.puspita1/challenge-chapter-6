package id.melur.binar.challengechapter6.helper

import android.content.Context
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import id.melur.binar.challengechapter6.database.Favorite
import id.melur.binar.challengechapter6.database.FavoriteDatabase
import id.melur.binar.challengechapter6.database.User
import id.melur.binar.challengechapter6.database.UserDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UserRepo(context: Context) {

    private val mDb = UserDatabase.getInstance(context)
    private val mDbFav = FavoriteDatabase.getInstance(context)

    suspend fun updateUser(username: String, name: String, dateOfBirth: String, address: String) = withContext(Dispatchers.IO) {
        mDb?.userDao()?.updateUser(username, name, dateOfBirth, address)
    }

    suspend fun insertUser(user: User) = withContext(Dispatchers.IO) {
        mDb?.userDao()?.insertUser(user)
    }

    suspend fun getRegisteredUser(username: String, password: String) = withContext(Dispatchers.IO) {
        mDb?.userDao()?.getRegisteredUser(username, password)
    }

    suspend fun getUser2(username: String) = withContext(Dispatchers.IO) {
        mDb?.userDao()?.getUser2(username)
    }

    suspend fun coba(username: String) = withContext(Dispatchers.IO) {
        mDb?.userDao()?.coba(username)
    }



    suspend fun insertFavorite(favorite: Favorite) = withContext(Dispatchers.IO) {
        mDbFav?.favoriteDao()?.insertFavorite(favorite)
    }

    suspend fun getRegisteredFavorite(username: String?, movieId: Int) = withContext(Dispatchers.IO) {
        mDbFav?.favoriteDao()?.getRegisteredFavorite(username, movieId)
    }

    suspend fun deleteFavorite(username: String?, movieId: Int) = withContext(Dispatchers.IO) {
        mDbFav?.favoriteDao()?.delete(username, movieId)
    }

    suspend fun test(username: String?) = withContext(Dispatchers.IO) {
        mDbFav?.favoriteDao()?.test(username)
    }
}