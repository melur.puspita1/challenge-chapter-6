package id.melur.binar.challengechapter6.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import id.melur.binar.challengechapter6.BuildConfig
import id.melur.binar.challengechapter6.R
import id.melur.binar.challengechapter6.viewmodel.ViewModel
import id.melur.binar.challengechapter6.adapter.MovieAdapter
import id.melur.binar.challengechapter6.databinding.FragmentFavoriteScreenBinding
import id.melur.binar.challengechapter6.helper.MovieRepo
import id.melur.binar.challengechapter6.helper.UserRepo
import id.melur.binar.challengechapter6.helper.viewModelsFactory
import id.melur.binar.challengechapter6.model.MoviePopular
import id.melur.binar.challengechapter6.model.MoviePopularItem
import id.melur.binar.challengechapter6.model.Status
import id.melur.binar.challengechapter6.service.TMDBApiService
import id.melur.binar.challengechapter6.service.TMDBClient
import id.melur.binar.challengechapter6.viewmodel.MovieViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FavoriteScreen : Fragment() {


    private var _binding: FragmentFavoriteScreenBinding? = null
    private val binding get() = _binding!!

    private lateinit var movieAdapter: MovieAdapter

    private lateinit var sharedPref: SharedPreferences

    private val userRepo : UserRepo by lazy { UserRepo(requireContext()) }

    private val apiService : TMDBApiService by lazy { TMDBClient.instance }
    private val viewModel: ViewModel by viewModelsFactory { ViewModel(userRepo, apiService) }

    private val movieRepository: MovieRepo by lazy { MovieRepo(apiService) }
    private val movieViewModel: MovieViewModel by viewModelsFactory { MovieViewModel(movieRepository) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentFavoriteScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getAllMovie()
        initRecyclerView()
    }


    private fun initRecyclerView() {
        movieAdapter = MovieAdapter { id: Int, movie: MoviePopularItem ->
            val bundle = Bundle()
            bundle.putInt("id", id)
            findNavController().navigate(R.id.action_favoriteScreen_to_detailScreen, bundle)
        }
        binding.rvFav.apply {
            adapter = movieAdapter
//            layoutManager = GridLayoutManager(requireContext(), 2)
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    fun getAllMovie() {
        apiService.getMoviePopular(BuildConfig.API_KEY)
            .enqueue(object : Callback<MoviePopular> {
                // kondisi get data berhasil dari http
                override fun onResponse(
                    call: Call<MoviePopular>,
                    response: Response<MoviePopular>
                ) {
                    // response.issuccessful sama dengan 200
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            movieAdapter.updateData(response.body()!!)
                        }
                    }
                    binding.pbMovie.isVisible = false
                }
                // kondisi get data gagal dari server/http, udh bener2 ga bisa di akses
                override fun onFailure(call: Call<MoviePopular>, t: Throwable) {
                    binding.pbMovie.isVisible = false
                }

            })
    }

}