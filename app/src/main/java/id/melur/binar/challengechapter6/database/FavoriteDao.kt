package id.melur.binar.challengechapter6.database

import androidx.room.*

@Dao
interface FavoriteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavorite(favorite: Favorite) : Long

    @Query("SELECT * FROM Favorite WHERE username = :username AND movieId = :movieId")
    fun getRegisteredFavorite(username: String?, movieId: Int) : List<Favorite>

    @Query("DELETE FROM Favorite WHERE username = :username AND movieId = :movieId")
    fun delete(username: String?, movieId: Int) : Int

    @Query("SELECT * FROM Favorite WHERE username = :username")
    fun test(username: String?) : List<Favorite>
}