package id.melur.binar.challengechapter6.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.BuildConfig
import com.bumptech.glide.Glide
import id.melur.binar.challengechapter6.BuildConfig.BASE_URL_IMAGE
import id.melur.binar.challengechapter6.databinding.ItemMovieBinding
import id.melur.binar.challengechapter6.model.MoviePopular
import id.melur.binar.challengechapter6.model.MoviePopularItem

class MovieAdapter(private val onClickListener : (id: Int, movie: MoviePopularItem) -> Unit) : RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<MoviePopularItem>() {
        override fun areItemsTheSame(oldItem: MoviePopularItem, newItem: MoviePopularItem): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: MoviePopularItem, newItem: MoviePopularItem): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private val listDiffer = AsyncListDiffer(this, diffCallback)

    fun updateData(movie: MoviePopular?) = listDiffer.submitList(movie?.results)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val binding = ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(listDiffer.currentList[position])
    }

    override fun getItemCount(): Int = listDiffer.currentList.size

    /**
     * view holder wajib extend RecyclerView ViewHolder
     * ViewHolder butuh view maka kita tambahkan parameter view
     *
     * untuk view binding
     * binding.root == view
     * jd kita bisa mengganti view dengan binding.root
     */
    inner class MovieViewHolder(private val binding: ItemMovieBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: MoviePopularItem) {
            binding.apply {
                tvTitle.text = item.title

                Glide.with(itemView.context)
                    .load("https://www.themoviedb.org/t/p/w220_and_h330_face/" + item.posterPath)
                    .into(ivPoster)
                itemMovie.setOnClickListener {
                    onClickListener.invoke(item.id, item)
                }

            }
        }
    }
}
