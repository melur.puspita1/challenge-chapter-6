package id.melur.binar.challengechapter6.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.melur.binar.challengechapter6.BuildConfig
import id.melur.binar.challengechapter6.database.Favorite
import id.melur.binar.challengechapter6.database.User
import id.melur.binar.challengechapter6.helper.UserRepo
import id.melur.binar.challengechapter6.model.MoviePopularItem
import id.melur.binar.challengechapter6.model.MovieResponse
import id.melur.binar.challengechapter6.service.TMDBApiService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ViewModel(private val userRepo: UserRepo, private val apiService: TMDBApiService) : ViewModel() {

    // di gunakan untuk assign value, hanya untuk di dalam view model
    private val _dataSuccess = MutableLiveData<MovieResponse>()

    // di gunakan untuk observe di fragment/activity
    val dataSuccess: LiveData<MovieResponse> get() = _dataSuccess


    private val _detailSuccess = MutableLiveData<MoviePopularItem>()
    val detailSuccess: LiveData<MoviePopularItem> get() = _detailSuccess

    private val _dataError = MutableLiveData<String>()
    val dataError: LiveData<String> get() = _dataError

    val user = MutableLiveData<List<User>>()
    val user2 = MutableLiveData<User?>()
    val isFav = MutableLiveData<Boolean>()
    val addDataUser = MutableLiveData<Boolean>()
    val isLogin = MutableLiveData<Boolean>()
    val usernamee = MutableLiveData<String>()
    val name = MutableLiveData<String>()
    val dateOfBirth = MutableLiveData<String>()
    val address = MutableLiveData<String>()
    val notRegistered = MutableLiveData<Boolean>()
    val validateEmailPassword = MutableLiveData<User?>()
    val update = MutableLiveData<Boolean>()
    val failUpdate = MutableLiveData<Boolean>()

    val test = MutableLiveData<Any>()

    fun getUser2(username: String) {
        viewModelScope.launch {
            user2.value = userRepo.getUser2(username)
        }
    }

    fun checkLogin(username: String, password: String) {
        viewModelScope.launch {
            val result = userRepo.getUser2(username)
            if (result == null) {
                notRegistered.value = true
            } else {
                if (result.username == username && result.password == password) {
                    validateEmailPassword.value = result
                }
            }
        }
    }

    fun checkRegisteredUser(username: String, password: String) {
        viewModelScope.launch {
            user.value = userRepo.getRegisteredUser(username, password)
            if (!user.value.isNullOrEmpty()) {
                isLogin.value = true
                usernamee.value = username
            }
        }
    }


    fun checkRegisteredFavorite(username: String?, movieId: Int){
        viewModelScope.launch {
            val result = userRepo.getRegisteredFavorite(username, movieId)
            CoroutineScope(Dispatchers.Main).launch {
                if (!result.isNullOrEmpty()) {
                    isFav.value = true
                }
            }
        }
    }


    fun addDataUser(username: String, email: String, password: String) {
        viewModelScope.launch {
            val user = User(null, username, email, password,"","","")
            userRepo.insertUser(user)
            addDataUser.value = true
        }
    }

    fun addDataFavorite(username: String?, movieId: Int) {
        viewModelScope.launch {
            val favorite = Favorite(null, username, movieId)
            userRepo.insertFavorite(favorite)
//            addDataUser.value = true
        }

    }

    fun deleteDataFavorite(username: String?, movieId: Int) {
        viewModelScope.launch {
            val favorite = Favorite(null, username, movieId)
            userRepo.deleteFavorite(username, movieId)
//            addDataUser.value = true
        }

    }

    fun test(username: String?) {
        viewModelScope.launch {
            val result = userRepo.test(username)
            test.postValue(result!!.elementAt(1))
//            addDataUser.value = true
        }
    }

    fun updateUser(username: String, name: String, dateOfBirth: String, address: String) {
        viewModelScope.launch {
            val result = userRepo.updateUser(username, name, dateOfBirth, address)
            if (result != 0) {
                update.value = true
            } else {
                failUpdate.value = true
            }
        }
    }

    fun coba(username: String) {
        viewModelScope.launch {
            val result = userRepo.coba(username)
            val test = result!!.elementAt(0)
            val etName = test.name
            val etDate = test.dateOfBirth
            val etAddress = test.address
            name.postValue(etName)
            dateOfBirth.postValue(etDate)
            address.postValue(etAddress)
        }
    }

    fun getDetailMovie(movieId: Int) {
        apiService.getDetailMovie(movieId = movieId, BuildConfig.API_KEY)
            .enqueue(object : Callback<MoviePopularItem> {
                override fun onResponse(
                    call: Call<MoviePopularItem>,
                    response: Response<MoviePopularItem>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            _detailSuccess.postValue(response.body())
                        } else {
                            _dataError.postValue("Datanya kosong")
                        }
                    } else {
                        _dataError.postValue("Pengambilan data gagal}")
                    }
                }

                override fun onFailure(call: Call<MoviePopularItem>, t: Throwable) {
                    _dataError.postValue("Server bermasalah")
                }
            })
    }

    fun getFavoriteMovie(movieId: Int) {
        apiService.getDetailMovie(movieId = movieId, BuildConfig.API_KEY)
            .enqueue(object : Callback<MoviePopularItem> {
                override fun onResponse(
                    call: Call<MoviePopularItem>,
                    response: Response<MoviePopularItem>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            _detailSuccess.postValue(response.body())
                        } else {
                            _dataError.postValue("Datanya kosong")
                        }
                    } else {
                        _dataError.postValue("Pengambilan data gagal}")
                    }
                }

                override fun onFailure(call: Call<MoviePopularItem>, t: Throwable) {
                    _dataError.postValue("Server bermasalah")
                }
            })
    }
}
